'use strict'

app = angular.module('opprt', ['ngResource', 'ngAnimate', 'ngRoute', 'opprtRoutes'])


app.factory 'WalletService', ($http) ->
  
  if !@fingerprint
    @fingerprint = new Fingerprint canvas:true

  @fp = @fingerprint.get()

  get_wallet: () =>
    fp = @fp
    $http 
      method:'GET'
      url: "/users/#{fp}"
    .success (data) ->
      console.log "Got wallet ... #{JSON.stringify(data)}" 
      data

  set_wallet: (wallet) =>
    console.log "Setting wallet to #{JSON.stringify(wallet)}"
    @wallet = wallet


  get_wallets: () =>
    $http 
      method: 'GET'
      url: '/wallets'
    .success (data) ->
      console.log "recvd wallets #{JSON.stringify(data)}"
      data

  store_wallets: (wallets) =>
    @wallets = wallets
  

  add_wallet: (wallet_id) =>
    self = @
    fingerprint = @fingerprint.get()
    wallet = 
      id: wallet_id
    $http.post "/users/#{fingerprint}", wallet
      .success (data, status, headers, config) ->
        data

  
  set_selected_wallet: (wallet_id) =>
    console.log "==>#{JSON.stringify(@wallet)}"
    @wallet.wallet = wallet_id

  get_selected_wallet: () =>
    @wallet.wallet

  str: () =>
    @fingerprint.get()

 
class @BaseCtrl
  @register: (name) ->
    console.log "App is #{app}"
    name ?= @name || @toString().match(/function\s*(.*?)\(/)?[1]
    console.log "registering controller #{name}"
    app.controller name, @
    console.log "registred controller #{name}"
 
  @inject: (args...) ->
    console.log "Injecting..."
    @$inject = args
 
  constructor: (args...) ->
    for key, index in @constructor.$inject
      @[key] = args[index]
 
    for key, fn of @constructor.prototype
      continue unless typeof fn is 'function'
      continue if key in ['constructor', 'initialize'] or key[0] is '_'
      @$scope[key] = fn.bind?(@) || _.bind(fn, @)
 
    @initialize?()


class MainController extends @BaseCtrl
  @register 'MainController'
  @inject '$scope', '$http', '$animate', '$location', '$compile', 'wallet', 'wallets', 'WalletService'


  initialize: ->
    
    if @wallet.data 
      @WalletService.set_wallet(@wallet.data)

    if @wallet.data.wallet # This means that we have a wallet, no need to show other wallets
      # $('#fp_label').text("#{@wallet.data.fp} / #{@wallet.data.wallet}")
      @$location.path('/login')
    else
      $('#fp_label').text(@WalletService.str())
      #now you gotta show other wallets
      element = $('#banks')
      for item in @wallets.data.items
        element.append @$compile("<h4 ng-click='wallet_selected(\"#{item.name}\");'>#{item.name}</h4>")(@$scope)
      element.owlCarousel()


  wallet_selected: (wallet_name) ->
    self = @
    @WalletService.add_wallet(wallet_name)
    .then (data) ->
      console.log "Data from add_wallet : #{JSON.stringify(data)}"
      self.WalletService.set_selected_wallet data.data.wallet
      self.$location.path '/login'
    
  

class LoginController extends @BaseCtrl
  @register 'LoginController'
  @inject '$scope', '$location', 'WalletService'

  initialize: ->
    console.log "Login to : #{@WalletService.get_selected_wallet()}"
    $('#wallet-name').text(@WalletService.get_selected_wallet())





