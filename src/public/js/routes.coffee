'use strict'

routes = angular.module('opprtRoutes', ['ngRoute'])
  .config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
    
    $locationProvider.html5Mode true

    $routeProvider
      .when '/signup',
        templateUrl:'partials/signup.jade'
        controller:'MainController'
      
      .when '/login',
        templateUrl: 'partials/login.jade'
        controller: 'LoginController'

      .when '/',
        templateUrl:'partials/index.jade'
        controller:'MainController'
        resolve:
          wallets: (WalletService) ->
            WalletService.get_wallets()
          wallet: (WalletService) ->
            WalletService.get_wallet()




      .when '/users/me',
        templateUrl: 'partials/profile.jade'
        controller: 'MainController'

      .otherwise 
        redirectTo:'/'
    ]


