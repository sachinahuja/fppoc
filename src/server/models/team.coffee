Model = require '../core/model'
class Team extends Model
	fields = ['name']
	dbFields = ['name']

	constructor: (data) ->
		super data, 'Team', fields, dbFields

module.exports = exports = Team