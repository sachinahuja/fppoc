'use strict'
	
express = require 'express'
router = express.Router()
MySQL = require '../core/mysql'


router.get '/:fp', (req, res) ->
	fp = req.params.fp
	console.log "Lets see if we have fingerprint #{fp}"
	MySQL.find 'fingerprints', 'fp', fp, (err, data) ->
		if err
			MySQL.db().query "insert into fingerprints set fp = #{fp}, created_date=utc_timestamp()", (error, data) ->
				if error
					res.error("Could not insert fingerprint")
				else
					res.json({id:fp})
		else
			console.log "Sending Back #{JSON.stringify(data)}"
			res.json(data)
			# if data.wallet
			# 	console.log "We have preferred wallet !! ==> #{data.wallet}"
			# 	res.json(data)
			# else
			# 	console.log "No wallet preference - return null"


# if this is being called, we ALREADY have a fingerprint in the database
router.post '/:fp', (req, res) ->
	wallet_id = req.body.id
	fingerprint = req.params.fp
	if wallet_id
		MySQL.db().query "update fingerprints set wallet = '#{wallet_id}', created_date=utc_timestamp() where fp = #{fingerprint}", (error, data) ->
			if error
				res.error("Some issue dealing with database: #{error}")
			else
				console.log "Active wallet stored"
				res.json({id:fingerprint, wallet:wallet_id})





router.get '/me', (req, res) ->
	res.json(req.user)


# router.post '/', passport.authenticate 'local-signup', {
# 		successRedirect: '/users/me'
# 		failureRedirect: '/signup'
# 		failureFlash: true
# 	}

# router.post '/', (req, res) ->
# 	controller.create req.body, res

# router.get '/:firstName/:lastName/:email', (req, res) ->
# 	controller.create()
# 	res.send "We'll create a User"

# router.post '/:id/up', (req, res) ->
# 	console.log "Rating up #{req.params.id}"

module.exports = exports = router