'use strict'

class MySQL


	constructor: () ->
		
		@mysql = require 'mysql'
		@pool = @mysql.createPool 
			host: 'localhost'
			user: 'root'
			password: 'root'
			database: 'mc_finger'
	db: () =>
		@pool

	

	find: (tableName, fieldName, fieldValue, callback) =>
		query = "SELECT * FROM #{tableName} WHERE #{fieldName}='#{fieldValue}'"
		@findOne query, callback
		

	findById: (tableName, id, callback) =>
		query = "SELECT * FROM #{tableName} WHERE ID=#{id}"
		@findOne query, callback

	findOne: (query, callback) =>
		@pool.query query, (err, rows) ->
			
			if err
				console.log "Error -- #{err}"
				callback err, null
			else if !rows || rows.length == 0
				callback new Error("No Records Found"), null
			else
				console.log "We got rows!"
				# callback null, MySQL.camelize(rows[0])
				callback null, rows[0]

	# Statics
	@changeCase = require 'change-case'
	@camelize: (data) ->
		cData = {}
		for key,val of data
			cKey = @changeCase.camel key
			cData[cKey] = val
		cData








module.exports = exports = new MySQL