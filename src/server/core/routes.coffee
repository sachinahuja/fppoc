'use strict'

module.exports = (app) ->
	console.log "Loading routes ..."

	app.get '/', (req, res) ->
		console.log "request recvd for root"
		res.render 'layout'

	app.get '/partials/:name', (req, res) ->
		console.log "recvd request for partial : #{req.params.name}"
		res.render "partials/#{req.params.name}"


	# Banks Array
	banks = 
		items: [
			{name: 'Citi'},
			{name: 'Wells Fargo'},
			{name: 'Bank of America'},
			{name: 'Capital One'},
			{name: 'MasterPass'},
			{name: 'Credit Union'},
			{name: 'TD Ameritrade'},
			{name: 'Apple Bank'},
			{name: 'Valley National'}

		]

	app.get '/wallets', (req, res) ->
		res.json(banks)  

	# app.get '/login', (req, res) ->
	# 	res.render 'login.jade', {message: req.flash('loginMessage')}

	# app.get '/signup', (req, res) ->
	# 	res.render 'signup.jade', {message: req.flash('signupMessage')}

	# app.post '/users', passport.authenticate 'local-signup', {
	# 	successRedirect: '/users/me'
	# 	failureRedirect: '/signup'
	# 	failureFlash: true
	# }

	# app.post '/login', passport.authenticate 'local-login', {
	# 	successRedirect: '/users/me'
	# 	failureRedirect: '/login'
	# 	failureFlash: true
	# }

	# app.get '/profile', isLoggedIn, (req, res) ->
	# 	res.render 'profile.jade', {user: req.user}

	# app.get '/logout', (req, res) ->
	# 	req.logout()
	# 	res.redirect('/')

	users = require '../routers/users'
	app.use '/users', users
	app



	
